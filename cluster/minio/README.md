kubectl -n libresh-system create secret generic --from-file=config.yaml object-storage-config
kubectl -n minio create secret generic --from-file=config.env prod-storage-configuration
kubectl -n minio apply -n tenant.yaml
kubectl -n minio apply -n ing.yaml