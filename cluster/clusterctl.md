# Install clusterctl

(These instructions are for reference, and not meant for automation, refer to original documentation for up to date commands)

## Install go

```
echo "export PATH=$PATH:/usr/local/go/bin" | sudo tee /etc/profile.d/gopath.sh > /dev/null
sudo apt install golang-go make
```

## Install outscale cluster api

https://github.com/outscale-dev/cluster-api-provider-outscale/blob/main/docs/src/topics/get-started-with-clusterctl.md

```
git clone https://github.com/outscale-dev/cluster-api-provider-outscale
cd cluster-api-provider-outscale
make install-clusterctl
./bin/clusterctl version
sudo mv ./bin/clusterctl /usr/local/bin/clusterctl
```

Please create $HOME/.cluster-api/clusterctl.yaml:

```
providers:
- name: outscale
  type: InfrastructureProvider
  url: https://github.com/outscale/cluster-api-provider-outscale/releases/latest/infrastructure-components.yaml
```

## Configure outscale credentials

```
export OSC_ACCESS_KEY=<your-access-key>
export OSC_SECRET_KEY=<your-secret-access-key>
export OSC_REGION=cloudgouv-eu-west-1
make credential
```

Configure [osc-cli](https://docs.outscale.com/en/userguide/Installing-and-Configuring-OSC-CLI.html) and make sure your bastion host can discuss with outscale API:

```
osc-cli api CreateApiAccessRule --IpRanges '["BastionIP/32"]'  --Description "Access from bastion-rizomo-prod"
```

(Full doc for [CreateApiAccessRule](https://docs.outscale.com/api?console#createapiaccessrule))