# Instructions to create production cluster for Rizomo

## Introduction

### Requirements

We assume that if you read this documentation, you have a basic understanding of the following concepts:

- linux
- linux containers (docker/runc/containerd/cri-o)
- APIs and declarative APIs
- kubernetes (k8s)
  - API
  - Controllers
  - Cluster API
- IaaS and cloud
  - outscale/aws
  - VM images
  - VPC/security-groups
  - load balancers
  - volumes
- CSI (Container Storage Interface)
- CNI (Container Network Interface)

### Goal

If you follow carefully these instructions, at the end, you will have a running instance of Rizomo on a kubernetes cluster running on outscale.

### Components

- To provision the base infrastructure we use [Outscale Cluster API provider](https://github.com/outscale-dev/cluster-api-provider-outscale).
- For the state, we rely on:
  - Outscale Object Store
  - [Zalando postgres operator](https://github.com/zalando/postgres-operator) to deploy HA postgres databases with backups
  - [Percona MongoDB operator](https://docs.percona.com/percona-operator-for-mongodb/index.html) to provide HA mongo databases with backups
- For the rest, we use [EOLE3](https://gitlab.mim-libre.fr/EOLE/eole-3/tools) to deploy:
  - nginx-ingress
  - cert-manager
  - keycloak
  - rizomo

## Getting started

### Bootstrap cluster

To get started, we need a k8s API, to be able to leverage Cluster API.
We’ll do that on [kind](kind.md) on the bastion host.

Then we can start our bootstrap cluster:

```
kind create cluster
kubectl cluster-info
```

### Install clusterctl

Once we have a running cluster, we need to prepare it to be able to create our workload cluster.
We need first to install [clusterctl](clusterctl.md).

Then we can make our bootstrap cluster ready to create workload clusters:
```
clusterctl init --infrastructure outscale
```

### Create workload cluster

We can now create a workload cluster, in our case Rizomo prod cluster.
(Create a ssh key beforehand in outscale,here it is with the name pierreozoux).

```
export OSC_REGION=cloudgouv-eu-west-1
export OSC_IOPS=150
export OSC_VOLUME_SIZE=50
export OSC_KEYPAIR_NAME=pierreozoux
export OSC_SUBREGION_NAME=cloudgouv-eu-west-1a
export OSC_VM_TYPE=tinav3.c2r4p2
export OSC_IMAGE_NAME=ubuntu-2004-2004-kubernetes-v1.22.11-2022-11-23
export OSC_VOLUME_TYPE=standard

clusterctl generate cluster \
    rizomo-prod \
    --kubernetes-version 1.22.11 \
    --control-plane-machine-count=3 \
    --target-namespace=rizomo-prod \
    --worker-machine-count=3 > ./cluster-rizomo-prod.yaml

kubectl create ns rizomo-prod
kubectl -n rizomo-prod apply -f cluster-rizomo-prod.yaml
```

This command is just a reference to explain how this file was created, but [cluster-rizomo-prod.yaml](cluster-rizomo-prod.yaml) is in this git repo, and is now disconnected from the way it was created.

### Get kubeconfig

```
export WORKLOAD_CLUSTER_KUBECONFIG=/tmp/workload-kubeconfig
clusterctl -n rizomo-prod get kubeconfig rizomo-prod > $WORKLOAD_CLUSTER_KUBECONFIG
export KUBECONFIG=$WORKLOAD_CLUSTER_KUBECONFIG
```

### Install CNI in workload cluster

[Cilium](https://docs.cilium.io) is now the defacto CNI for kubernetes, relying on eBPF for networking and monitoring of networks.

First, we’ll need to install [cilium](cilium-cli.md) cli.

Then, we can install cilium:
```
helm repo add cilium https://helm.cilium.io/
wget https://raw.githubusercontent.com/syself/cluster-api-provider-hetzner/main/templates/cilium/cilium.yaml
helm upgrade --install cilium cilium/cilium --version 1.12.4 --namespace kube-system -f cilium.yaml 
cilium status --wait
cilium connectivity test
```

This command is just a reference to explain how cilium.yaml was created, but [cilium.yaml](cilium.yaml) is in this git repo, and is now disconnected from the way it was created.

### Install CCM in workload cluster

CCM is the Cloud Controller Manager, it allows to keep in sync VMs tags, and create cloud load balancer when you create k8s load balancer services.

```
git clone https://github.com/outscale-dev/cloud-provider-osc
cd cloud-provider-osc/
cp deploy/secrets.example.yml secrets.yaml
vi secrets.yaml # and edit accordingly
helm upgrade --install -n kube-system --wait --wait-for-jobs k8s-osc-ccm deploy/k8s-osc-ccm --set oscSecretName=osc-secret
osc-cli api CreateApiAccessRule --IpRanges '["80.247.14.168/32"]' # get the IP of the Internet Gateway of the VPC of the cluster
```

### Install CSI in workload cluster

CSI is the container storage interface, and it allows to create and map volumes to the right VM in your cloud provider when you create k8s PVCs, and bind them to k8s nodes.

```
git clone https://github.com/outscale-dev/osc-bsu-csi-driver.git
cd osc-bsu-csi-driver/
git checkout v1.1.0
helm upgrade --install osc-bsu-csi-driver ./osc-bsu-csi-driver     --namespace kube-system     --set enableVolumeScheduling=true     --set enableVolumeResizing=true     --set enableVolumeSnapshot=true     --set region=cloudgouv-eu-west-1
cp ./examples/kubernetes/storageclass/specs/example.yaml ./storage-class.yaml
kubectl apply -f ./storage-class.yaml
```

This command is just a reference to explain how storage-class.yaml was created, but [storage-class.yaml](storage-class.yaml) is in this git repo, and is now disconnected from the way it was created.

### EOLE 3 - preflight

We use [eole3](https://gitlab.mim-libre.fr/EOLE/eole-3/tools) to deploy nginx-ingress and cert-manager:

```
git clone https://gitlab.mim-libre.fr/EOLE/eole-3/tools.git && cd tools
sudo apt-get install jq
pip install click jinja2
```

copy [vars.ini.sample](cluster-prod/vars.ini.sample) to `vars.ini` and use the eole command to generate templates and deploy the components to kubernetes:
```
./build gen-socle
./install/deploy
```

### Move bootstrap/mgmt cluster into workload cluster

The procedure is documented [upstream here](https://cluster-api-cloudstack.sigs.k8s.io/topics/mover.html).

First, you need to initialize the workload cluster to be able to host:

```
export WORKLOAD_CLUSTER_KUBECONFIG=/tmp/workload-kubeconfig
clusterctl -n rizomo-prod get kubeconfig rizomo-prod > $WORKLOAD_CLUSTER_KUBECONFIG
export KUBECONFIG=$WORKLOAD_CLUSTER_KUBECONFIG
cd cluster-api-provider-outscale
export OSC_ACCESS_KEY=<your-access-key>
export OSC_SECRET_KEY=<your-secret-access-key>
export OSC_REGION=cloudgouv-eu-west-1
make credential
clusterctl init --infrastructure outscale
```

And then, you just can move:
```
unset KUBECONFIG
clusterctl -n rizomo-prod describe cluster rizomo-prod
clusterctl -n rizomo-prod move --to-kubeconfig /tmp/workload-kubeconfig -v10 --dry-run
clusterctl -n rizomo-prod move --to-kubeconfig /tmp/workload-kubeconfig -v10
export KUBECONFIG=$WORKLOAD_CLUSTER_KUBECONFIG
clusterctl -n rizomo-prod describe cluster rizomo-prod
```

## Backup the cluster

The idea is to store all the state in buckets.
For databases, dumps, wal and/or physical snapshots are sent to buckets, we call this the first line.

We assume that at this point, you have a [rw oos user](https://gitlab.mim-libre.fr/rizomo/cluster-prod/-/tree/main/rizomo/preprod#create-aksk-for-oos), and another oos user that we'll call ro, and we'll configure everything in this section.

The tools (aws cli, and mc cli mainly) are configured with the read/write profile (rw) and read only (ro) profile.

First, you need to figure out the USER ID of both.
Create a random test bucket for that:
```
mc mb rw/rwrandombucket
mc mb ro/rorandombucket
```

And then save the USER ID in a variable:
```
rw_id=`aws s3api get-bucket-acl --profile rw --bucket rwrandombucket --endpoint https://oos.cloudgouv-eu-west-1.outscale.com | jq -r '.Grants[0].Grantee.ID'`
ro_id=`aws s3api get-bucket-acl --profile ro --bucket rorandombucket --endpoint https://oos.cloudgouv-eu-west-1.outscale.com | jq -r '.Grants[0].Grantee.ID'`
```

Then, for each prod bucket, you have to grant ro to ro user:
```
mc ls rw | cut -d" " -f 9 | cut -d'/' -f1 | xargs -L1 -I% aws s3api put-bucket-acl --profile rw --bucket % --endpoint https://oos.cloudgouv-eu-west-1.outscale.com --grant-full-control "id=${rw_id}" --grant-read "id=${ro_id}"
```

You can verify that this works with this command:
```
mc ls rw | cut -d" " -f 9 | cut -d'/' -f1 | xargs -L1 -I% aws s3api get-bucket-acl --profile rw --bucket % --endpoint https://oos.cloudgouv-eu-west-1.outscale.com 
```
