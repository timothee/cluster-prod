# Install Percona Mongodb Operator

(These instructions are for reference, and not meant for automation, refer to original documentation for up to date commands)

https://docs.percona.com/percona-operator-for-mongodb/kubernetes.html

```
cd percona-server-mongodb-operator/
kubectl -n rizomo-preprod apply -f deploy/rbac.yaml
kubectl -n rizomo-preprod apply -f deploy/operator.yaml
```
