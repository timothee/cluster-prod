# Install production Rizomo

## Create buckets

Ask a OOS account to Outscale.
You need to specify a valid email and the user ID of the outscale account to attach it to.
Then, on the cockpit, you reset the pass associated with your email, enter the cockpit and get the AK/SK in the UI.

Install and configure [aws cli](https://docs.outscale.com/en/userguide/Installing-and-Configuring-AWS-CLI.html) for outscale.

```
aws s3api create-bucket \
    --profile oos \
    --bucket rizomo-prod \
    --acl public-read \
    --endpoint https://oos.cloudgouv-eu-west-1.outscale.com

aws s3api create-bucket \
    --profile oos \
    --bucket rizomo-backups \
    --endpoint https://oos.cloudgouv-eu-west-1.outscale.com
```

## Postgres

Then, we install a HA postgres instance:

```
helm repo add postgres-operator-charts https://opensource.zalando.com/postgres-operator/charts/postgres-operator --set configKubernetes.enable_pod_antiaffinity=false
helm repo update
helm install postgres-operator postgres-operator-charts/postgres-operator
kubectl apply -f ./pg.yml
```

Edit `vars.ini` to add keyclaok credentials that you can find in the secret named `postgres.pg-keycloak.credentials.postgresql.acid.zalan.do`

## Mongo

Install [Percona Mongo Operator](mongo.md).

Then we can deploy our HA mongodb instance:
```
export MONGO_PASSWORD=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 18 | head -n 1)
kubectl -n rizomo-prod create secret generic rizomo-app --from-literal=MONGO_PASSWORD=$MONGO_PASSWORD
kubectl -n rizomo-prod apply -f ./mongo.yaml
```

Edit `vars.ini` to add keyclaok credentials that you can find in the secret named `postgres.pg-keycloak.credentials.postgresql.acid.zalan.do`

## Secrets

Secrets are managed in cluster, but we still need to init them.

Copy the folder `../secrets-example` to a folder named `/path/to/rizomo/secrets` on your work environement and populate them.

## Keycloak

And then use helm to deploy Keycloak:

```
helm upgrade --install -n rizomo-prod keycloak codecentric/keycloak -f kc.yaml -f /path/to/rizomo/secrets/prod/kc.yaml
```

## Rizomo

To deploy Rizomo, you can use helm:
```
kubectl -n rizomo-prod apply -f ../rizomo-cm.yaml
helm upgrade --install laboite eole/laboite -n rizomo-prod -f rizomo.yaml -f /path/to/rizomo/secrets/prod/rizomo.yaml
```

## Questionnaire
```
helm upgrade --install questionnaire eole/questionnaire -n rizomo-prod -f ./questionnaire.yaml
```

And then, in keycloak, in the sso client, add questionaire url to the list of valid redirect URIs.
