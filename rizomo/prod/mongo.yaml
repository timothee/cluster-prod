apiVersion: psmdb.percona.com/v1
kind: PerconaServerMongoDB
metadata:
  name: mongo-rizomo-prod
  namespace: rizomo-prod
  finalizers:
    - delete-psmdb-pods-in-order
spec:
  crVersion: 1.13.0
  image: percona/percona-server-mongodb:5.0.11-10
  imagePullPolicy: Always
  allowUnsafeConfigurations: false
  updateStrategy: SmartUpdate
  upgradeOptions:
    versionServiceEndpoint: https://check.percona.com
    apply: disabled
    schedule: "0 2 * * *"
    setFCV: false
  secrets:
    users: mongodb-users
  pmm:
    enabled: false
  replsets:
  - name: rs0
    size: 3
    affinity:
      antiAffinityTopologyKey: "kubernetes.io/hostname"
    podDisruptionBudget:
      maxUnavailable: 1
    expose:
      enabled: false
      exposeType: ClusterIP
    resources:
      limits:
        memory: "3G"
      requests:
        memory: "0.5G"
    volumeSpec:
      persistentVolumeClaim:
        resources:
          requests:
            storage: 5Gi
    nonvoting:
      enabled: false
      size: 0
    arbiter:
      enabled: false
      size: 0
  sharding:
    enabled: false
  backup:
    enabled: true
    image: percona/percona-backup-mongodb:1.8.1
    serviceAccountName: percona-server-mongodb-operator
    resources:
      requests:
        memory: "0.5G"
    storages:
      oos:
        type: s3
        s3:
          bucket: rizomo-backups
          credentialsSecret: rizomo-s3
          region: cloudgouv-eu-west-1
          prefix: "mongodb"
          endpointUrl: https://oos.cloudgouv-eu-west-1.outscale.com
    pitr:
      enabled: true
      oplogSpanMin: 10
      compressionType: gzip
      compressionLevel: 6
    tasks:
      - name: daily-oos
        enabled: true
        schedule: "1 3 * * *"
        keep: 7
        storageName: oos
        compressionType: gzip
        compressionLevel: 6
      - name: weekly-oos
        enabled: false
        schedule: "3 1 * * 0"
        keep: 12
        storageName: oos
        compressionType: gzip
        compressionLevel: 6
---
---
apiVersion: batch/v1
kind: Job
metadata:
  name: rizomo-prod-configure-mongo
  namespace: rizomo-prod
spec:
  template:
    spec:
      containers:
      - command: [/init/configure-mongo.sh]
        env:
        - name: FQDN
          value: mongo-rizomo-prod
        - name: NS
          value: rizomo-prod
        envFrom:
        - secretRef:
            name: rizomo-app
        - secretRef:
            name: mongodb-users
        image: 'mongo:4'
        imagePullPolicy: IfNotPresent
        name: configure
        volumeMounts:
        - name: init-script
          mountPath: /init/
      volumes:
      - name: init-script
        configMap:
          name: configure-mongo
          defaultMode: 0700
      restartPolicy: Never
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: configure-mongo
data:
  configure-mongo.sh: |
    #!/bin/bash -eux
    export host="mongodb://${MONGODB_USER_ADMIN_USER}:${MONGODB_USER_ADMIN_PASSWORD}@${FQDN}-rs0-0.${FQDN}-rs0.${NS}.svc.cluster.local:27017,${FQDN}-rs0-1.${FQDN}-rs0.${NS}.svc.cluster.local:27017,${FQDN}-rs0-2.${FQDN}-rs0.${NS}.svc.cluster.local:27017/rizomo-prod?authSource=admin&replicaSet=rs0"
    mongo --host=$host --eval "db.getSiblingDB('admin');"
    mongo --host=$host --eval "db.getSiblingDB('admin').createUser({user: 'rizomo-prod',pwd: \"$MONGO_PASSWORD\", roles: [{ role: 'readWrite', db: 'rizomo-prod' }, {role: 'clusterMonitor', db: 'admin'}]});"
