# Migration process: Scalingo to K8s

This document provides instructions for migrating a system from Scalingo to K8s. The instructions cover migrating the Keycloak and Laboite services, configuring services, and cleaning up after the migration.
## Table of Contents 
- [Migrate Keycloak](#Migrate-Keycloak) 
- [Migrate Laboite](#Migrate-Laboite) 
- [Configure Services](#Configure-Services) 
- [Cleanup](#Cleanup)
## Migrate Keycloak

This section provides instructions for migrating the Keycloak service.
### Summary

The Keycloak migration process involves the following steps:
1. Retrieve the PostgreSQL database URL using Scalingo CLI. 
2. Save the retrieved database URL into the `DATABASE_URL` variable. 
3. Dump the database using `pg_dump` with the specified options.
4. Get the superuser credentials from Kubernetes secrets.
5. Run a debug pod in a separate shell.
6. Copy the dumped database file to the debug container.
7. Install PostgreSQL in the debug pod.
8. Connect to the PostgreSQL instance using the retrieved superuser credentials. 
9. Perform the following operations in the PostgreSQL shell:
- Revoke connection privileges to the 'keycloak' database from the public.
- Terminate all active connections to the 'keycloak' database.
- Rename the old 'keycloak' database to 'keycloak_bak'.
- Create a new 'keycloak' database.
- Grant connection privileges back to the public for both 'keycloak' and 'keycloak_bak' databases.
- Exit the PostgreSQL shell. 
10. Restore the dumped database using `pg_restore`.
11. Restart all Keycloak-related pods in order to clear cache.

### Instructions

1. Retrieve the PostgreSQL database URL using Scalingo CLI:

```shell
scalingo --region osc-secnum-fr1 --app rizomo-keycloak env | grep POSTGRESQL
```



This command fetches the environment variables for the specified app and filters out the PostgreSQL database URL. 
2. Save the retrieved database URL into the `DATABASE_URL` variable:

```shell
export DATABASE_URL="postgresql://<username>:<password>@<host>:<port>/<db>"
```

 
3. Dump the database using `pg_dump` with the specified options:

```shell
pg_dump --clean --if-exists --format c --dbname $DATABASE_URL --no-owner --no-privileges --no-comments --exclude-schema 'information_schema' --exclude-schema '^pg_*' --file keycloak.pgsql
```


4. Get the superuser credentials from Kubernetes secrets:

```shell
printf "\nPassword: "
kubectl get secrets/postgres.pg-keycloak.credentials.postgresql.acid.zalan.do --template={{.data.password}}  | base64 -D
printf "\nUsername: "
kubectl get secrets/postgres.pg-keycloak.credentials.postgresql.acid.zalan.do --template={{.data.username}}  | base64 -D ; printf "\n"
```


5. Run a debug pod in a separate shell:

```shell
kubectl run -i --rm --tty debug --image=ubuntu --restart=Never -- bash
```


> ⚠️ Warning: If you exit the debug pod you will need to restart from here
6. Copy the dumped database file to the debug container:

```shell
kubectl cp keycloak.pgsql debug:/tmp -c debug
```


7. Install PostgreSQL in the debug pod:

```shell
apt update
apt install postgresql postgresql-contrib -y
```

8. Connect to the PostgreSQL instance using the retrieved superuser credentials:

```shell
psql -h pg-keycloak -U <username> -W
```

 
9. Perform the following operations in the PostgreSQL shell:

a. Revoke connection privileges to the 'keycloak' database from the public:

```sql
REVOKE CONNECT ON DATABASE keycloak FROM public;
```



b. Terminate all active connections to the 'keycloak' database:

```sql
SELECT pid, pg_terminate_backend(pid) 
FROM pg_stat_activity 
WHERE datname = 'keycloak' AND pid <> pg_backend_pid();
```



c. Rename the old 'keycloak' database to 'keycloak_bak':

```sql
ALTER DATABASE keycloak RENAME TO keycloak_bak;
```



d. Create a new 'keycloak' database:

```sql
CREATE DATABASE keycloak;
```



e. Grant connection privileges back to the public for both 'keycloak' and 'keycloak_bak' databases:

```sql
GRANT CONNECT ON DATABASE keycloak TO public;
GRANT CONNECT ON DATABASE keycloak_bak TO public;
```



f. Exit the PostgreSQL shell:

```css
\q
``` 
10. Restore the dumped database using `pg_restore`:

```shell
pg_restore --clean --if-exists --no-owner --no-privileges --no-comments -d keycloak -h pg-keycloak -U <username> -W keycloak.pgsql
```


11. Restart all Keycloak-related pods in order to clear cache:

```shell
kubectl delete pods keycloak-0 keycloak-1 pg-keycloak-0 pg-keycloak-1
```


## Migrate Laboite

This section provides instructions for migrating the Laboite service.
### Summary

The Laboite migration process involves the following steps:
1. Retrieve the MongoDB URL using Scalingo CLI. 
2. Save the retrieved MongoDB URL into the `MONGO_URI` variable. 
3. Dump the database using `mongodump` with the specified options.
4. Copy the dumped database file to the debug container.
5. Install MongoCLI in the debug pod.
6. Connect to the MongoDB instance using the retrieved admin credentials.
7. Create a temporary migration user.
8. Backup the database.
9. Import the new database.

### Instructions

1. Retrieve the MongoDB URL using Scalingo CLI:

```shell
scalingo --region osc-secnum-fr1 --app rizomo-prod env | grep MONGO_URL
export MONGO_URI="mongodb://<username>:<password>@<host>:<port>"
```

 
2. Dump the database using `mongodump` with the specified options:

```shell
curl -LO https://db-api.osc-secnum-fr1.scalingo.com/api/ca_certificate

mongodump --uri "$MONGO_URI" --db rizomo-prod --ssl --sslCAFile ca_certificate -o mongodump
```


3. Copy the dumped database file to the debug container:

```shell
kubectl cp mongodump debug:/tmp -c debug
```

4. Install MongoCLI in the debug pod:

```shell
apt-get install wget gpg -y

wget -qO - https://www.mongodb.org/static/pgp/server-6.0.asc | gpg --dearmor -o /usr/share/keyrings/mongodb-archive-keyring.gpg

echo "deb [signed-by=/usr/share/keyrings/mongodb-archive-keyring.gpg] https://repo.mongodb.org/apt/ubuntu focal/mongodb-org/6.0 multiverse" | tee /etc/apt/sources.list.d/mongodb-org-6.0.list

apt-get update
apt-get install -y mongodb-database-tools mongodb-mongosh
```


5. Connect to the MongoDB instance using the retrieved admin credentials:

```shell
printf "\nURI: "
kubectl get secrets/mongodb-settings --template={{.data.mongo_url}}  | base64 -D
printf "\nPassword: "
kubectl get secrets/mongodb-users --template={{.data.MONGODB_USER_ADMIN_PASSWORD}}  | base64 -D
printf "\nUsername: "
kubectl get secrets/mongodb-users --template={{.data.MONGODB_USER_ADMIN_USER}}  | base64 -D ; printf "\n"
```


6. Connect to the MongoDB instance using the retrieved admin credentials:

```shell
mongosh "<URI>" -u "<Username>" -p "<Password>"
```


7. Create a temporary migration user:

```shell
> db.createUser({ user: "migration", pwd: "<newPassword>", roles: ["root"] });
```


8. Backup the database:

```shell
mongodump -d rizomo-prod -o mongodump/ --uri "$MONGO_URI" --ssl --sslCAFile ca_certificate
mongorestore -d rizomo-prod-bak "$MONGO_URI" mongodump/rizomo-prod --username migration --password "<newPassword>"
mongosh "$MONGO_URI" --username migration --password "<newPassword>"
> use rizomo-prod
> db.dropDatabase();
> exit
```


9. Import the new database:

```shell
mongorestore -d rizomo-prod "$MONGO_URI" mongodump/rizomo-prod --username migration --password "<newPassword>"
```


## Configure Services

This section provides instructions for configuring services after the migration.
### Summary

The configuration process involves updating the Keycloak and Laboite service configurations.
### Instructions

Update Keycloak auth service config:

```shell
mongosh "$MONGO_URI" --username migration --password "<newPassword>"
> use rizomo-prod
> db.meteor_accounts_loginServiceConfiguration.update({}, { $set: { serverUrl: "https://snap-auth.numerique.gouv.fr/auth" }})
> exit
```



Update Keycloak client config: 
1. Connect to [https://snap-auth.numerique.gouv.fr/](https://snap-auth.numerique.gouv.fr/) .
2. Access the "Rizomo" realm.
3. Go to "Clients".
4. Select "SSO". 
5. Add "[https://rizomo-prod.numerique.gouv.fr/](https://rizomo-prod.numerique.gouv.fr/) *" to the *Valid Redirect URIs* field.
6. Save.


## Cleanup

This section provides instructions for cleaning up after the migration.
### Summary

The cleanup process involves removing the temporary migration user.
### Instructions

Remove the temporary migration user:

```shell
mongosh "$MONGO_URI" --username migration --password "<newPassword>"
> use admin
> db.dropUser("migration")
> exit
```



Exit the debug pod:

```shell
exit
```

Check if the debug pod is still up (it can happen when you leave the shell without entering `exit` on the first time):

```shell
kubectl get pods debug
```



If the debug pod is still running, delete it:

```shell
kubectl delete pod debug
```

# Post Scriptum

AI Generated documentation from https://codimd.mim-libre.fr/dS3n4Wj4R7iFdDQW-5S7XA
